/* 
    atmega328_hal_timer.h - HAL for ATMEGA328 Timers  
    Copyright (C) 2016  Terrance Hendrik,  terrance \dot hendrik \at gmail \dot com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef atmega328_hal_timer_h_included
#define atmega328_hal_timer_h_included


#define TIMER_OUTPUT_NC 0
#define TIMER_OUTPUT_TG 1
#define TIMER_OUTPUT_POS 2
#define TIMER_OUTPUT_NEG 3

#define TIMER16_MODE_NORMAL          0
#define TIMER16_MODE_PWMPC8          1
#define TIMER16_MODE_PWMPC9          2
#define TIMER16_MODE_PWMPC10         3
#define TIMER16_MODE_CTC_OCR1A       4
#define TIMER16_MODE_FPWM8           5
#define TIMER16_MODE_FPWM9           6
#define TIMER16_MODE_FPWM10          7
#define TIMER16_MODE_PWMPFC_ICR1     8
#define TIMER16_MODE_PWMPFC_OCR1A    9
#define TIMER16_MODE_PWMPC_ICR1      10
#define TIMER16_MODE_PWMPC_OCR1A     11
#define TIMER16_MODE_CTC_ICR1        12
#define TIMER16_MODE_RFU             13
#define TIMER16_MODE_FPWM_ICR1       14
#define TIMER16_MODE_FPWM_OCR1A      15

#define TIMER01_CLKSRC_NONE           0
#define TIMER01_CLKSRC_CLKIO_DIV1     1
#define TIMER01_CLKSRC_CLKIO_DIV8     2
#define TIMER01_CLKSRC_CLKIO_DIV64    3
#define TIMER01_CLKSRC_CLKIO_DIV256   4
#define TIMER01_CLKSRC_CLKIO_DIV1024  5
#define TIMER01_CLKSRC_T1PIN_F        6
#define TIMER01_CLKSRC_T1PIN_R        7

#define ICRES1  6
#define ICNC1   7

#define TIMER8_MODE_NORMAL          0
#define TIMER8_MODE_PWMPC8          1
#define TIMER8_MODE_CTC_OCRA        2
#define TIMER8_MODE_FPWM8           3
#define TIMER8_MODE_PWMPC_OCRA      5
#define TIMER8_MODE_FPWM_OCR1A      7

#define TIMER2_CLKSRC_NONE           0
#define TIMER2_CLKSRC_CLKT2S_DIV1    1
#define TIMER2_CLKSRC_CLKT2S_DIV8    2
#define TIMER2_CLKSRC_CLKT2S_DIV32   3
#define TIMER2_CLKSRC_CLKT2S_DIV64   4
#define TIMER2_CLKSRC_CLKT2S_DIV128  5
#define TIMER2_CLKSRC_CLKT2S_DIV256  6
#define TIMER2_CLKSRC_CLKT2S_DIV1024 7

#define ASSR_EXCLK      0x40
#define ASSR_AS2        0x20
#define ASSR_TCN2UB     0x10
#define ASSR_OCR2AUB    0x08
#define ASSR_OCR2BUB    0x04
#define ASSR_TCR2AUB    0x02
#define ASSR_TCR2BUB    0x01

#ifdef TCCR0B
inline
void setupT0(uint8_t mode, uint8_t clksel, uint8_t outputA, uint8_t outputB, uint8_t foca, uint8_t focb) {
  TCCR0A = (outputA << 6) | (outputB << 4) | (mode & 0x03);
  TCCR0B = (foca << 7) | (focb << 6) | (mode >> 2 << 3) | clksel;
}
#endif


inline
void setupT1(uint8_t mode, uint8_t clksel, uint8_t outputA, uint8_t outputB, uint8_t noiseCanceler, uint8_t edgeSel) {
  TCCR1A = (outputA << 6) | (outputB << 4) | (mode & 0x03);
  TCCR1B = (noiseCanceler << 7) | (edgeSel << 6) | (mode >> 2 << 3) | clksel;
}

#ifdef TCCR2B
inline
void setupT2(uint8_t mode, uint8_t clksel, uint8_t outputA, uint8_t outputB, uint8_t foca, uint8_t focb) {
  TCCR2A = (outputA << 6) | (outputB << 4) | (mode & 0x03);
  TCCR2B = (foca << 7) | (focb << 6) | (mode >> 2 << 3) | clksel;
}
#endif

#endif


