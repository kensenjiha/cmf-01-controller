/*
    crc_ccitt16_xmodem.h - CRC16 X-25 , optimized for performance
    Copyright (C) 2017  Terrance Hendrik terrance.hendrik@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 
    Revision:
        v20170418, for CCITT
        v20181018, for xmodem 

*/


#ifndef _CRC16_XMODEM_H_
#define _CRC16_XMODEM_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

void
crc16_xmodem_init();

void
crc16_xmodem_update(uint8_t *buf, size_t len);

uint16_t
crc16_xmodem_result();

uint16_t
crc16_xmodem(uint8_t *buf, size_t len);

#ifdef __cplusplus
}
#endif

#endif


