#include <avr/power.h>
#include <avr/sleep.h>
#include "atmega328_hal_timer.h"

#include "crc16_xmodem.h"
#include <avr/pgmspace.h>


#if 0
// for Terrance board
  #define STICK_0 A11
  #define STICK_1 A12
#else
// for Arduino Uno
  #define STICK_0 A0
  #define STICK_1 A1
#endif

#define MAX_ACCEL 0x800

#define LOAD_LIM 8000

volatile int16_t omega = 0;
volatile int16_t omega_target = 0;

volatile static uint16_t phase = 0;

volatile bool toSend = false;

#define SAMP_RATE 100L
#define T1_TOP (F_CPU / 64 / SAMP_RATE - 1)

ISR(TIMER1_OVF_vect) {  
// if OCR1A <65536, it never overflows
}

ISR(TIMER1_COMPA_vect) {  
  // smooth speed change
  if (omega_target != omega){
    if (omega_target > omega){
      omega += min(MAX_ACCEL, omega_target - omega);
    } else if (omega_target < omega){
      omega -= min(MAX_ACCEL, omega - omega_target);
    }
  }
  phase += omega;
  toSend = true;
}

void setup() {
  Serial.begin(115200);
//  test
//  Serial.println("CMF-01 controller test");
//  Serial.println(__DATE__);
//  {
//    uint16_t c = crc16_xmodem("\x01",1);
//    Serial.println(c);
//  }

  delay(1000);

  setupT1(TIMER16_MODE_CTC_OCR1A, TIMER01_CLKSRC_CLKIO_DIV64, TIMER_OUTPUT_NC, TIMER_OUTPUT_NC, 0, 0);
  OCR1A = T1_TOP;
  TIMSK1 = _BV(OCIE1A);

//  Serial.println(T1_TOP+1);
  omega = 0;
  omega_target = 0;

  pinMode(13, OUTPUT); // Overload indicator
  pinMode( 2, OUTPUT); // Power/Wakeup signal, connect to CMF-01's Charger port as 5V or 3V
  digitalWrite(2, HIGH);
}

uint8_t idxRx = 0;
uint8_t idxTx = 0;

uint8_t rxFrame[32];
uint8_t txFrame[32];

uint8_t txF2[32];


// 0xA5 0x5A, frame header
// 0x45, [7:4] = 4 register operation, [3:0] = 5 payload length
// 0x01, [3:0] = 1 "Read register"
// 0x88, register address
// 0x85, 0x86: angle?
// 0x87 angle error
// 0x88 motor torque/current
// 0x00, 0x00: don't care for "Read"
// 0x00, 0x00: CRC16 to be calculated
// Stored in FLASH/ROM
const PROGMEM uint8_t  READ_A[10] = {0xa5, 0x5a, 0x45, 0x01, 0x88, 0x00, 0x00, 0x00, 0x00, 0x00};

void
sendAngle(uint16_t ang){
  static uint8_t ctr = 0;
  uint16_t crc;

  ctr++;
  txFrame[ 0] = 0xa5;  // header A5 5A
  txFrame[ 1] = 0x5a;
  txFrame[ 2] = 0x0a;  // [7:4] 0, actuation command 
  txFrame[ 9] = ang & 0xff;        // angle LSB
  txFrame[10] = (ang >> 8) & 0xff; // angle MSB, each inc/dec 0x4000 rotates 1 full revolution

  crc = crc16_xmodem(txFrame, 13); // frame checksum
  txFrame[13] = crc & 0xff;
  txFrame[14] = (crc >> 8) & 0xff;

  for (uint8_t i = 0; i < 15; i++){
    Serial.write(txFrame[i]);
  }

  for (uint8_t i = 0; i < 10 - 2; i++){
    txF2[i] = pgm_read_byte(&(READ_A[i]));
  }
  crc = crc16_xmodem(txF2, 8);
  txFrame[8] = crc & 0xff;
  txFrame[9] = (crc >> 8) & 0xff;
  for (uint8_t i = 0; i < 10; i++){
    Serial.write(txFrame[i]);
  }
}

// max 16
#define ADC_NSUM 16
#define CENTER_ADC (512*ADC_NSUM)
#define FRAC_RANGE (CENTER_ADC / 4)

void loop() {
  if (Serial.available()) {
    uint8_t c = Serial.read();
    rxFrame[idxRx] = c;
    idxRx++;
    switch(idxRx){
      case 1:
        if (0xa5 != c) {
          idxRx = 0;
        }
        break;
      case 2:
        if (0x5a != c) {
          idxRx = 0;
        }
        break;
      case 3:
        if (0x40 != (c & 0xf0)) {  // NOT "Read register" result
          idxRx = 0;
        }
        break;
      case 10:
        if (0x45 == rxFrame[2]){
          int16_t load = (int16_t)rxFrame[6] | ((int16_t)rxFrame[7] << 8);
          digitalWrite(13, HIGH);
          // TODO configurable LOAD_LIM
          if (load > LOAD_LIM){
            phase -= (load - LOAD_LIM) / 4;
          } else if (load < -LOAD_LIM){
            phase += (LOAD_LIM - load) / 4;
          } else {
            digitalWrite(13, LOW);
          }
          idxRx = 0;
        }
        break;
      case 15:
        if (0x85 == rxFrame[2]){
          // Nothing to do
        }
        idxRx = 0;
        //rxFrame[0] = 0;
        break;
      default:
        break;
    } // switch
  } // if (Serial.available())
  
  if(toSend){
    toSend = false;
    sendAngle(phase);
  }

  // read joy stick
  { 
    static uint16_t vstick0 = 0;
    static uint16_t vstick1 = 0;
    static uint8_t count = 0;
    int16_t o0 = 0;
    vstick0 += analogRead(STICK_0);
    vstick1 += analogRead(STICK_1);
    count++;
    if (ADC_NSUM == count){
      uint16_t ff_0 = abs((int16_t)vstick0 - CENTER_ADC);
      //ff_0 = ff_0 * 7 / 8; // Speed scale limit
      if (ff_0 > FRAC_RANGE) { // deadzone
        bool rev_0 = vstick0 < CENTER_ADC;
        uint8_t exp_0 = ff_0 / FRAC_RANGE;
        uint16_t frac_0 = ff_0 % FRAC_RANGE;
        if (exp_0 < 1){ // +/- 1x (0 / 2x)
          o0 = 0; //??
        } else {
          o0 = (((uint32_t)frac_0 + FRAC_RANGE) << (exp_0+4)) / FRAC_RANGE;
        }
        if(rev_0){
          o0 = -o0;
        }
        omega_target = o0;
      } else /* if (1.0 != stick_scale)*/{
        omega_target = 0;
      }
      // finally
      vstick0 = 0;
      vstick1 = 0;
      count = 0;
    }// if (ADC_NSUM == count)
  }
}


