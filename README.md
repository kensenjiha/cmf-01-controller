# CMF-01 controller

Arduino based/compatible Zhiyun Crane2 CMF-01 focus follower controller. Using potentiometers to control focus/zoom speed. Configurable torque limit.

## connection

* USB Micro-B OTG port (at the long flat side) (marked +Camera)

Connect to Arduino's USB port using USB OTG cable.

* Charger/CRANE2 'USB' Micro-B port

Pin 1 to Arduino D2, for wakeup. But, be aware, trickiely, this port does __NOT__ **keep motor alive**.

Pin 2 to Arduino D1, TXD of Arduino

Pin 3 to Arduino D0, RXD of Arduino

Pin 5 to GND

## TO DO list

* Design a controller board
* Configurable torque limit
* Power control (Drive Arduino D2 Low)
* Camera control, maybe